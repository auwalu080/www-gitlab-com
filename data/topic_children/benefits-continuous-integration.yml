title: What is continuous integration (CI)?
seo_title: What is continuous integration (CI)?
description: Learn about continuous integration and its benefits and see why
  organizations that use continuous integration have a competitive advantage
  over those that don’t.
header_body: Learn about continuous integration and its benefits and see why
  organizations that use continuous integration have a competitive advantage
  over those that don’t.
canonical_path: /topics/ci-cd/benefits-continuous-integration/
file_name: benefits-continuous-integration
parent_topic: ci-cd
twitter_image: /images/opengraph/ci-cd-opengraph.png
cover_image: /images/topics/g_gitlab-ci-cd.svg
body: >-
  ## Continuous integration (CI) explained


  [Continuous integration, or CI](/topics/ci-cd/), is the practice of integrating all your code changes into the main branch of a shared source code repository early and often, automatically testing each change when you commit or merge them, and automatically kicking off a build. CI helps DevOps teams detect and resolve conflicts early and ensures that the codebase remains stable. CI is a key practice for Agile development teams.


  The ultimate goal of CI is to deliver working code quickly and securely. Before you get started, there are two things you should keep in mind:


  **First, deliver code in small iterations.** Most software projects, even in small organizations, will involve a number of features that are being worked on by different team members. Even in the best case scenario, it can be difficult for team members to have visibility into what others are working on. This gets even worse if developers work on separate feature branches in isolation, and only merge to the main branch when their work is complete — and when the time comes to merge everyone’s work, everything from code conflicts to unexpected security concerns will delay the release. But if each developer pushes their updates to the main branch a little bit at a time, the team will be well on the way to achieving CI, with fewer conflicts and more predictable releases. Software development processes such as [feature flags](https://docs.gitlab.com/ee/operations/feature_flags.html) can help teams deliver new features to users quickly and safely.


  **Second, set up automated testing to keep your code safe and secure.** Long ago, the “build” and “test” phases of software development existed in isolation, with code being checked for security vulnerabilities only after it was complete and ready for release. An essential part of CI is continuous testing — testing the code for vulnerabilities throughout the development process. But as you might guess, this might be difficult to achieve manually. That’s where automated testing comes in. Today’s CI tools take the code pushed by each developer and run tests, such as unit tests or integration tests, automatically. 


  ## What are the benefits of continuous integration for DevOps teams?

  CI makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. Software developers get feedback on their code sooner, increasing the overall pace of innovation.


  Organizations that adopt continuous integration have [a competitive advantage](/blog/2019/06/27/positive-outcomes-ci-cd/) because they can deploy faster. Organizations that have implemented CI are making revenue on the features they deploy, not waiting for manual code checks.


  Studies done by DevOps Research and Assessment (DORA) have shown that robust DevOps practices lead to improved [business outcomes](https://cloud.google.com/devops/state-of-devops/). All of these DORA 4 metrics can be improved by using CI:


  * **Lead time**: Early feedback and build/test automation help decrease the time it takes to go from code committed to code successfully running in production.

  * **Deployment frequency**: Automated tests and builds are a prerequisite to automated deploy.

  * **Time to restore service**: Automated pipelines enable fixes to be deployed to production faster, reducing mean time to resolution (MTTR).

  * **Change failure rate**: Early automated testing greatly reduces the number of defects that make their way to production.


  ## The business benefits of continuous integration

  By eliminating manual tasks, DevOps teams can work more efficiently and with greater speed. An automated workflow also improves handoffs, which enhances overall operational efficiency. The business benefits of continuous integration allow organizations to:


  * **Iterate faster**: Smaller code changes allow software development teams to iterate faster and are easier to manage.

  * **Find problems easily**: Teams can find problems in code because all code is managed and tested in smaller batches.

  * **Improve transparency**: Continuous feedback through frequent testing helps developers see where bugs are.

  * **Reduce costs**: Automated testing frees up time for developers by reducing manual tasks, and better code quality means fewer errors and less downtime.

  * **Make users happy**: Fewer bugs and errors make it into production, so users have a better experience.


  Automated testing reduces the chances of human error and ensures that only code that meets certain standards makes it into production. Because code is tested in smaller batches, there’s less context-switching for developers when a bug or error occurs. Pipelines can also identify *where* the error occurs, making it easier to not only identify problems, but fix them.


  A dev environment with fewer manual tasks means that engineers can spend more time on revenue-generating projects. With fewer errors, teams are more efficient and spend less time putting out fires. When processes such as unit testing are automated, engineers are happier and can focus on where they add the most value.


  ## How to set up continuous integration

  To set up CI for your project, here are some steps to follow:


  First, [choose a version control and CI tool](/features/continuous-integration/) that allows developers to push frequently and test continuously while reducing context switching.


  Then set up a shared central repository for your code and configure your CI tool to monitor your repository for changes and automatically build and test your code each time a change is detected.


  Finally, configure your CI tool to automatically deploy your code to a staging or production environment if the tests pass.


  Once you’ve set up CI, your team can focus on writing code and committing changes to the shared repository. The CI tool will handle the rest, automatically building, testing, and deploying your code. This can save your team time and reduce the risk of errors in your software.


  ## Continuous integration (CI) vs. continuous delivery (CD)

  CI goes hand in hand with continuous delivery, or CD. Together, [continuous integration and delivery (CI/CD)](/topics/ci-cd/) bring [automation into the DevOps lifecycle](/blog/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/). Organizations that implement CI/CD make better use of their resources, are more cost efficient, and allow developers to focus on innovation.


  There are a few key differences between CI and CD. For one, CI generally happens more frequently than CD. CI is also typically used to refer to the process of automating the build and testing of code changes, while CD generally refers to the process of automating the release of code changes.


  * **Continuous integration (CI)** is the practice of merging all developer working copies to a shared mainline several times a day.

  * **Continuous delivery (CD)** is a software development practice where code changes are automatically built, tested, and deployed to production.
  

cta_banner:
  - cta:
      - url: /stages-devops-lifecycle/continuous-integration/
        text: Learn More
    title: Continuous Integration (CI) with GitLab
    body: Powerful automation to build and test faster at any scale
resources_title: More on continuous integration
resources:
  - url: https://about.gitlab.com/customers/hotjar/
    type: Case studies
    title: How Hotjar deploys 50% faster with GitLab
  - url: /resources/ebook-single-app-cicd/
    type: Books
    title: The benefits of a single application CI/CD
